// Simple MasterMind game, Bekir Murat 2015
// Single user plays against the program.

#include "ui.h"
#include "globals.h"
#include "colorcode.h"
#include "player.h"
#include "game.h"

int main( int argc, const char* argv[] )
{
    // Init
    using namespace exercise;
    MasterMindCmdLineUI ui;
    Player player(ui);
    MasterMind game (player, ui);

    game.Init();

    // Main loop
    while( true )
    {
        game.Start();

        if(! player.WantsToPlayAgain() ) break;
    }

    player.Notify("Thank you for playing, bye !");

    return 0;
}

