#pragma once
#include <string>

namespace exercise
{
    class UserInterface;

    /** This is the user. */
    class Player
    {
        int m_score = 0;
        UserInterface& m_ui;
    public:
        Player(UserInterface& ui);

        /** Score. */        
        int GetScore () const;
        void AddScore( int score );

        /** Notify a text to the user, whatever the backend. */       
        void Notify( std::string text );

        /** Ask the user if they want to continue playing, return their answer. */
        bool WantsToPlayAgain();
    };

} // exercise

