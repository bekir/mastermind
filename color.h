#pragma once

#include <vector>
#include <iostream>

namespace exercise
{
    /** Represents the possible colors for a peg. */
    enum class Color : char
    {
        Undefined = 'U',
        Red = 'R',
        Green = 'G',
        Blue = 'B',
        Yellow = 'Y',
        Orange = 'O',
        White = 'W'
    };

    /* Useful to print out the colors easily. */
    std::ostream& operator << ( std::ostream& os, Color color );

    /* Helper to check if a char is one of our colors. */
    bool IsAColor( char c );

    /* Helper that returns a vector of all the possible colors. */
    std::vector<Color>& PossibleColors();

} //exercise

