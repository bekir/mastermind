#pragma once
#include "colorcode.h"

namespace exercise
{
    class Player;
    class MasterMindUI;

    /** Generic game interface. */
    class Game
    {
    public:

        /** Init the game. */
        virtual void Init() = 0;

        /** Start to play. */        
        virtual void Start() = 0;
    };

    /** Our mastermind game. */
    class MasterMind : public Game
    {
        /** Generated code that the player will try to discover. */
        ColorCode m_secretCode;
        Player& m_player;

        /** Main way of interacting with the user. */        
        MasterMindUI& m_ui;

        /** Number of tries in a game. */
        int m_maxTries;

    public:
        /** We need a user and a user interface to be able to play. */
        MasterMind(Player& player, MasterMindUI& ui);
        virtual ~MasterMind () {}        

        /** See Game interface. */        
        void Init();
        void Start();
    };

} // exercise

