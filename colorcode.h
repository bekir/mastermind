#pragma once

#include <vector>
#include <iostream>
#include "color.h"

namespace exercise
{
    /** Code composed by a given number of colors in a specific order. */
    class ColorCode
    {
        std::vector<Color> m_pegs;
    public:

        /* Useful to print out codes. */
        friend std::ostream& operator << ( std::ostream& os, const ColorCode& code );
        
        /* Useful to print out codes. */
        std::string ToString() const;

        /* Gives random color values to the code. */
        void Randomize();

        /* Add a color to the code. */
        void Add( Color col );

        /* Returns true if the code looks good. */
        bool IsValid();

        /* Compares code to another one and returns two metrics :
           - number of exact color matches (at correct position),
           - number of correct colors in the wrong position. */
        std::pair<int, int> CompareColors( const ColorCode& toCompare );
    };

} // exercise

