#pragma once
#include <string>

namespace exercise
{
    class ColorCode;

    /** This is a generic interface for user interaction. */
    class UserInterface
    {
    public:

        /** Returns true if the user wants to continue playing. */
        virtual bool WantsToPlayAgain() = 0;
        
        /** Notify user of a message. */
        virtual void Notify( std::string& text ) = 0;
    };

    /** Specific user interaction for MasterMind game, but still backend agnostic. */
    class MasterMindUI : public UserInterface
    {
    public:

        /** Returns the number of guesses the user wants to be able to do on each game. */
        virtual int NumberOfGuesses() = 0;

        /** Returns a formatted guess made by the user. */
        virtual ColorCode Guess() = 0;
    };

    /** Command line backend for the MasterMind game. */
    class MasterMindCmdLineUI : public MasterMindUI
    {
    public:
        virtual ~MasterMindCmdLineUI() {}

        /** See UserInterface. */
        bool WantsToPlayAgain();

        /** See UserInterface. */
        void Notify( std::string& text );

        /** See MasterMindUI. */
        int NumberOfGuesses();

        /** See MasterMindUI. */
        ColorCode Guess();
    };
} // exercise

