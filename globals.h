#pragma once

namespace exercise
{
    /** Size of the codes used during the game (number of color pegs). */
    const int SizeOfCode = 4;

    /** Number of possible colors in the game
        (modifying this would imply modifications in color.h). */
    const int NumberOfColors = 6;
} // exercise

