#include <iostream>
#include <string>
#include "ui.h"
#include "colorcode.h"
#include "color.h"
#include "globals.h"

namespace exercise
{
    bool MasterMindCmdLineUI::WantsToPlayAgain()
    {
        std::string str;
        std::cout << std::endl << "Do you want to play again ? (Y/N)" << std::endl;
        std::getline( std::cin, str );
        if( str == "Y" ) return true;
        return false;
    }

    void MasterMindCmdLineUI::Notify( std::string& text )
    {
        std::cout << text << std::endl;
    }

    int MasterMindCmdLineUI::NumberOfGuesses()
    {
        std::string str;
        int guesses, defaultNb( 9 );
        std::cout << std::endl << "How many guesses do you want to make for each code ? (default is 9)" << std::endl;
        std::getline( std::cin, str );
        if( str == "" ) return defaultNb;
        try
        {
            guesses = std::stoi( str );
        }
        catch( std::invalid_argument& )
        {
            return defaultNb;
        }
        if( guesses > 0 ) return guesses;
        return defaultNb;
    }

    ColorCode MasterMindCmdLineUI::Guess()
    {
        // Prompt
        std::cout << std::endl << "Please make a guess (" << SizeOfCode << " uppercase characters with no separator) :" << std::endl;
        std::cout << "( Available colors :";
        for( auto& color : PossibleColors() ) std::cout << " " << color;
        std::cout << " )" << std::endl;
        
        // Loop until the answer is correct
        while( true )
        {
            ColorCode set;
            std::string str;
            std::cin.clear();
            std::getline( std::cin, str );
            if( str.length() != SizeOfCode )
            {
                std::cout << "Sorry, wrong format, please try again" << std::endl;
            }
            else
            {
                for( auto c : str )
                {
                    if( IsAColor( c ) )
                    {
                        set.Add( Color( c ) );
                    }
                    else
                    {
                        std::cout << "Sorry, " << c << " is not one of our valid colors, please try again" << std::endl;
                        break;
                    }
                }
                // Return the first correct answer
                if( set.IsValid() ) return set;
            }
        }
    }
} // exercise

