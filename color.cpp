#include "color.h"

namespace exercise
{
    std::ostream& operator << ( std::ostream& os, Color color )
    {
        return os << static_cast<char>( color );
    }

    bool IsAColor( char c )
    {
        if (( c == static_cast<char>( Color::Red ) )
            || ( c == static_cast<char>( Color::Green ))
            || ( c == static_cast<char>( Color::Blue ))
            || ( c == static_cast<char>( Color::Yellow ))
            || ( c == static_cast<char>( Color::Orange ))
            || ( c == static_cast<char>( Color::White )))
            return true;

        return false;
    }

    std::vector<Color>& PossibleColors()
    {
        static std::vector<Color> all = {	Color::Red,
                                            Color::Green,
                                            Color::Blue, 
                                            Color::Yellow, 
                                            Color::Orange, 
                                            Color::White };
        return all;
    }

} //exercise

