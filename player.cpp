#include "player.h"
#include "ui.h"

namespace exercise
{
    Player::Player(UserInterface& ui) : m_ui(ui)
    { }
    
    int Player::GetScore () const
    {
        return m_score;
    }

    void Player::AddScore( int score )
    {
        m_score += score;
    }

    void Player::Notify( std::string text )
    {
        m_ui.Notify( text );
    }

    bool Player::WantsToPlayAgain()
    {
        return m_ui.WantsToPlayAgain();
    }

} // exercise
