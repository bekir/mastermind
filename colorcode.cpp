#include <chrono>
#include <algorithm>
#include "colorcode.h"
#include "color.h"
#include "globals.h"

namespace exercise
{
    std::string ColorCode::ToString() const
    {
        std::string result;

        for( auto& c : m_pegs )
        {
            result += static_cast<char> (c);
        }

        return result;
    }

    void ColorCode::Randomize()
    {
        // First wipe out the current code
        m_pegs.clear();

        // Randomly choose color pegs
        srand( static_cast<unsigned int> (std::chrono::system_clock::now().time_since_epoch().count()) );
        std::vector<Color>& all = PossibleColors();
        for( int i = 0; i < SizeOfCode; ++i )
        {
            m_pegs.push_back( all[rand() % NumberOfColors] );
        }
    }
    
    void ColorCode::Add( Color col )
    {
        m_pegs.push_back( col );
    }

    bool ColorCode::IsValid()
    {
        // Just a basic check (we could also check the validity of each element)
        return m_pegs.size() == SizeOfCode;
    }

    std::pair<int, int> ColorCode::CompareColors( const ColorCode& toCompare )
    {
        int rightPlace( 0 );
        int wrongPlace( 0 );

        if (m_pegs.empty() || toCompare.m_pegs.empty() ||
            (m_pegs.size() != toCompare.m_pegs.size()))
            return std::make_pair( rightPlace, wrongPlace );

        // Local copy to be able to remove elements
        std::vector<Color> other = toCompare.m_pegs;
        
        // Check colors at the correct positions and wrong positions
        for( int j = 0; j < SizeOfCode; ++j )
        {
            // Correct position
            if( other[j] == m_pegs[j] )
            {
                ++rightPlace;
                other[j] = Color::Undefined;
                continue;
            }

            // Correct color, but wrong position
            auto it = std::find( other.begin(), other.end(), m_pegs[j] );
            while( it != other.end() )
            {
                // Have to loop to make sure we do not misclassify colors
                if( other[j] != m_pegs[j] )
                {
                    ++wrongPlace;
                    *it = Color::Undefined;
                    break;
                }
                
                it = std::find( it, other.end(), m_pegs[j] );
            }
        }

        // Return results
        return std::make_pair( rightPlace, wrongPlace );
    }

    std::ostream& operator << ( std::ostream& os, const ColorCode& code )
    {
        os << code.ToString();
        return os;
    }
} // exercise

