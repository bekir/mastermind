#include <tuple>
#include <string>
#include "game.h"
#include "player.h"
#include "ui.h"
#include "colorcode.h"
#include "globals.h"

namespace exercise
{
    MasterMind::MasterMind(Player& player, MasterMindUI& ui) : m_player(player), m_ui(ui)
    {
        player.Notify( "Welcome to MasterMind" );
    }

    void MasterMind::Init()
    {
        // Ask for parameters
        m_maxTries = m_ui.NumberOfGuesses();
    }

    void MasterMind::Start()
    {
        // Points earned in this game
        int points( 0 );
        
        // Randomly choose colors
        m_secretCode.Randomize();
        m_player.Notify( "Secret code generated, your turn now !" );

        // DEBUG
        // std::cout << m_secretCode << std::endl;

        // Loop on player guesses
        for( int tries = 1; tries <= m_maxTries; ++tries )
        {
            m_player.Notify( "ROUND " + std::to_string(tries) + "/" + std::to_string(m_maxTries) );
            
            // Ask player to guess
            ColorCode guess = m_ui.Guess();

            // Compare to secret code
            int coloursRightPlace(0), coloursWrongPlace(0);
            std::tie(coloursRightPlace, coloursWrongPlace) = m_secretCode.CompareColors(guess);

            // Success
            if(coloursRightPlace == SizeOfCode)
            {
                // Calculate points
                points = m_maxTries - tries + 1;
                
                // Greetings
                m_player.Notify( "You found the code, well done !" );
                break;
            }
            // Not success : show the results
            else
            {
                m_player.Notify( "Correct colors at the wrong place : " + std::to_string( coloursWrongPlace ) );
                m_player.Notify( "Correct colors at the right place : " + std::to_string( coloursRightPlace ) );
            }
        }

        // Process points and player's final score
        m_player.AddScore(points);
        m_player.Notify( "Points gained : " + std::to_string( points ) );
        m_player.Notify( "Your overall score is : " + std::to_string(m_player.GetScore()) );
    }

} // exercise

